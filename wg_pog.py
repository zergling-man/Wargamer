#General imports
import pygame
#Package imports
import wg_globals as glo

class Pog:

 def __init__(self, type, x, y, bp=True):
  self.x,self.y=x,y
  self.type=type
  self.image=glo.getimage(type)
  self.xa,self.ya=self.image.get_rect().width/2,self.image.get_rect().height/2
  self.bp=bp
  glo.objs.append({'obj':self,'type':'ac'})

 def draw(self, screen):
  #Aliases
  x,y=self.x,self.y
  xa,ya=self.xa,self.ya
  if self.bp:
   back=pygame.Surface((xa*2,ya*2))
   back.fill(glo.bgcol)
   screen.blit(back,(x-xa,y-ya,x+xa,y+ya))
  screen.blit(self.image,(x-xa,y-ya,x+xa,y+ya))