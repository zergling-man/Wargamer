#General imports
import pygame, os

#The game's background colour.
bgcol=(200,170,40)

#Unit art assets
unitart={}
def artimport():
 for a,b,c in os.walk("Images"):
  for image in c:
   unitart[image[:-4]]=pygame.image.load(a+'/'+image).convert_alpha()
#And a special function for it
def getimage(str):
 out=unitart.get(str,-1)
 if out==-1:
  out=unitart['pogmissing']
 return out

#The list of existing objects
#This is important for checking click events and whatnot.
#Note that every entry is a dictionary.
objs=[]

#A stub, I use this for creating empty methods.
def stub():
 print("Stub!")