Board:
The game takes place on a 'map', a large area with a hexagonal grid inside it.
Terrain pieces may be placed to cover parts of the grid.
Any hex that does not have at least half its space both inside the grid and uncovered is considered unusable.
Starting zones of 10 hexes are to be assigned to each player.

Selection:
Your army must be entirely one race.
You have 30 selection points.
Commanders cost 10, elites cost 4, soldiers cost 2, summons cost 1.
You must have at least one commander.
You cannot have more than one of the same commander.
Pick your first commander in secret, then reveal it at the same time.
Then pick the rest of your army in secret.
A tiebreaking token is to be given to a random player before the beginning of the first turn.

Turn order:
Each player has 8 cards, numbered from 1 to 8.
Place four of those face-down in front of you. Left to right, these are the speeds of commanders, elites, soldiers and summons. Higher number means faster speed.
You then play in four phases, of the unit types. In each phase, all players flip up the relevant speed card.
Any players who are tied must reveal a card from their hand to break the tie with other relevant players. If they tie again, the player who is closest to the tiebreaker token counterclockwise is considered faster.
Once ties have been resolved, players move all units, in order of fastest player to slowest. Then the next phase begins.
Once all phases are complete, a new turn begins, and all cards are collected up and replaced, and the tiebreaker token moves one player to the left.
You are required to place a card for phases that you don't have units in, but you do not participate in their tiebreakers.

Each phase:
All units except summons have two actions per turn. Summons have one.
An action is an attack or a move.
Abilities can be used at any time during that unit's turn.
Using an ability does NOT consume an action.
Units must take their entire turns at once.
Movespeed is 3 hexes per action.
Combat is simply the damage is dealt to the target's health.
When a unit has 0 or less health, it dies.

Death penalties:
Commanders have a death penalty, a morale-break style thing, when they die, your army suffers the stated penalty.

ABILITIES
-------
Summon X Y:	You may place a summon of type Y from the reserve pool adjacent to the unit, up to the limit of X.
Healing X Y:	Heal X health to Y units nearby. (Adjacent)
Revive X:	When killed, unit respawns on next turn, unless corpse is killed. Corpse has X health.
Combine:	Deals double damage if adjacent to another Combined Attack.
Active:		Unit can take an extra action each turn.
Armour X:	Reduces incoming damage by X.
Poison X:	On attack, unit is given X poison, no stack.
		Deals damage then reduces by one at the start of the receiver's turn.
Range:		Allows the unit to attack from a range, using the same system as movement for determining targets.
Phasewalk:	Ignores unit collisions (both ways). Can only attack its own hex, and can be attacked safely from its hex.
		Attacking into that hex attacks both units.
Smite X:	If an attack would leave an opponent on X health or less, it instead kills them.
Rebound:	Return to the space the unit started the turn on.
Charge:		Instead of taking action(s), make one movement in a straight line then attack for double damage.
Backstab:	Deals double damage if there is an allied unit directly opposite the target.
Crit:		At the beginning of a game turn, rolls 50% chance to deal double damage this turn
Reflect:	Attacks any unit that attacks it, if able. (If it could make an attack against them normally.)