#General imports
import pygame, sys, random
#Package imports
import wg_fpscounter as fps, wg_globals as glo, wg_grid as gr, wg_pog as pog, wg_input as inp

def draw(screen):
 screen.fill(glo.bgcol)
 for derp in glo.objs:
  derp['obj'].draw(screen)
 pygame.display.update()

pygame.init()
screen=pygame.display.set_mode((1280,960))

glo.artimport()

counter=fps.FPSCounter(30,screen.get_rect().width, 1, bp=False)
counter.align=2
counter.set_col((0,0,255),glo.bgcol)

grid=gr.Grid(11,6,45)

derp2=pog.Pog("bowman",150,100, False)
i=0
pogs=[]
while i<10:
 pogs.append(pog.Pog("bowman",random.random()*screen.get_rect().width,random.random()*screen.get_rect().height, False))
 i+=1

while 1:
 for event in pygame.event.get():
  if event.type==pygame.QUIT:
   pygame.quit()
   sys.exit()
  if event.type==pygame.MOUSEBUTTONUP:
   x,y=event.pos
   inp.click(x,y,event.button)
  if event.type==pygame.KEYDOWN:
   inp.keyb(event.key)
 if inp.holding!=False:
  inp.holding.x,inp.holding.y=pygame.mouse.get_pos()
 counter.update()
 draw(screen)