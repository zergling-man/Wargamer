#General imports
import pygame
#Package imports
import wg_globals as glo

class FPSCounter:

 def __init__(self, size, x=0, y=0, rate=1, bp=True, bg=True):
  self.times=[pygame.time.get_ticks()]
  #The font size seems to be about 2/3 the value in pixels for whatever reason. This fixes it...
  self.font=pygame.font.Font(None,int(size*1.5))
  self.fpswidth=0
  self.x,self.y=x,y
  self.size=size
  self.align=0
  self.updaterate=rate
  self.tick=1000/rate
  self.changed={'true':0}
  #Defaults to yellow on black. FraPS lives on.
  self.col=(255,255,0)
  self.bgcol=(0,0,0)
  self.fps=self.font.render("0", 0, self.col)
  #This is needed for collision functions
  self.xa,self.ya=self.fps.get_rect().width/2,self.fps.get_rect().height/2
  self.bp=bp
  if bg:
   glo.objs.append({'obj':self,'type':'bg'})
  else:
   glo.objs.append({'obj':self,'type':'ac'})

 def set_col(self, col, bgcol):
  self.col=col
  self.bgcol=bgcol

 def set_pos(self, x, y):
  self.x,self.y=x,y

 def set_align(self, align):
  self.align=align

 def set_bp(self, bp=True):
  self.bp=bp

 def set_size(self, size):
  #The font size seems to be about 2/3 the value in pixels for whatever reason. This fixes it...
  self.font=pygame.font.Font(None,int(size*1.5))
  self.size=size

 def set_rate(self, rate):
  self.updaterate=rate
  self.tick=1000/rate

 def update(self):
  ret=0
  times=self.times
  blah=pygame.time.get_ticks()
  if times[0]+self.tick<blah:
   #Get the frames since last update, use the rate to convert it to "per second"
   frames=len(times)*self.updaterate
   #Then get the text ready to draw
   self.fps=self.font.render(str(frames), 0, self.col)
   #This is needed for collision functions
   self.xa,self.ya=self.fps.get_rect().width/2,self.fps.get_rect().height/2

   #Empty the frame list ready for next update.
   times[:]=[]
   ret=1
  times.append(blah)
  return ret

 def draw(self, screen):
  self.clearold(screen)
  #Aliases
  size=self.size
  x,y=self.x,self.y
  width=self.fpswidth
  times=self.times
  self.fpswidth=width=self.fps.get_rect().width
  if self.bp:
   back=pygame.Surface((width,size))
   back.fill(self.bgcol)

  #Get the right alignment
  (xa,ya)=self.apos((x,y,size,width),self.align)

  #Draw it.
  if self.bp:
   screen.blit(back,(xa,ya,width,size))
  screen.blit(self.fps,(xa,ya,width,size))

  self.backup()

 def clearold(self, screen):
  changed=self.changed
  sparex = changed.get('x',self.x)
  sparey = changed.get('y',self.y)
  spareal = changed.get('align',self.align)
  sparesi = changed.get('size',self.size)
  x,y=self.apos((sparex,sparey,sparesi,self.fpswidth),spareal)
  back=pygame.Surface((self.fpswidth,sparesi))
  back.fill(glo.bgcol)
  screen.blit(back,(x,y,self.fpswidth,sparesi))

 def backup(self):
  changed=self.changed
  changed['x']=self.x
  changed['y']=self.y
  changed['align']=self.align
  changed['size']=self.size

 def apos(self, vals, switch):
  x,y,size,width=vals
  #This hackish line will get the right alignment system based on the flag.
  #defaults to 0 if something weird is specified
  return {0:(x,y), 1:(x,y-size), 2:(x-1-width,y), 3:(x-1-width,y-size)}.get(switch,(x,y))