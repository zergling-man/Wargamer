#General imports
import pygame
#General aliases
draw=pygame.draw
#Package imports
import wg_globals as glo

class Grid:

 def __init__(self, x, y, radius):
  self.radius=radius
  #Each horizontal hex tessellates 1/4 of it over the last 1/4 of the last hex
  #So it only adds 3/4 of its size each time.
  #Except, of course, for the first hex which adds its full size.
  #...However, because size is specified in radius, which is half its total width
  #We double the value.
  self.width=width=x*radius*1.5+radius*0.5+2
  self.x=x
  #The above is much the same for height, although this will be too tall if x is 1.
  #I don't care about that too much.
  #Also it tessellates half its height instead of only 1/4.
  self.height=height=y*radius*2+radius+2
  self.y=y
  #The magic numbers above (2) are because it cuts off the edges otherwise.
  #This line is quite obvious by contrast.
  self.grid=grid=pygame.Surface((width,height))
  grid.set_colorkey((255,255,255))
  grid.fill((255,255,255))
  self.makegrid()
  glo.objs.append({'obj':self,'type':'bg'})

 def makegrid(self):
  i=0
  radius=self.radius
  while i<self.x:
   j=0
   while j<self.y:
    #y-adjust
    ya=0
    if i%2==1:
     ya=radius
    self.drawhex(self.grid,i*radius*1.5+radius,j*radius*2+radius+ya)
    j+=1
   i+=1

 def draw(self, screen):
  screen.blit(self.grid,(0,0,self.width,self.height))

 def drawhex(self, canvas, x, y):
  radius=self.radius
  #Was going to use a loop.
  #Realised replacing it with the (fixed) six lines
  #Would save a ton of math.
  ##Lazyway
  col=(0,0,0)
  #Top (wraith) line
  draw.line(canvas,col,(x-radius*0.5,y-radius),(x+radius*0.5,y-radius),2)
  #Top-left line
  draw.line(canvas,col,(x-radius,y),(x-radius*0.5,y-radius),2)
  #Bot-left line
  draw.line(canvas,col,(x-radius*0.5,y+radius),(x-radius,y),2)
  #Bot line
  draw.line(canvas,col,(x-radius*0.5,y+radius),(x+radius*0.5,y+radius),2)
  #Bot-right line
  draw.line(canvas,col,(x+radius,y),(x+radius*0.5,y+radius),2)
  #Top-right line
  draw.line(canvas,col,(x+radius,y),(x+radius*0.5,y-radius),2)
  #Oh yes, the direction is not consistent. I don't care.
  #How can you tell if a line is forward or backward anyway?